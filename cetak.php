<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Herugroupschool</title>
    <link rel="shortcut icon" href="herugroups.png">
    <link href="dashboard.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="dashboard.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous"></head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
<body>

 <center>

  <h2>DAFTAR SISWA YANG TERDAFTAR</h2>

 </center>

 <div class="table-responsive">
           <?php include("crud.php"); ?>
  <nav>
  <a href="index.php" class="btn btn-primary mb-2">Home</a>
  </nav>
  <br>
  <table class="table table-striped">
  <thead>
    <tr>
    <th>No</th>
    <th>Nama Lengkap</th>
    <th>Tempat Lahir</th>
    <th>Tanggal Lahir</th>
    <th>Jurusan</th>
    <th>Jenis Kelamin</th>
    <th>Email</th>
    <th>No Handphone</th>
    </tr>
  </thead>
  <tbody>
    <?php
        $sql ="SELECT * FROM calon_siswa";
        $query =mysqli_query($db, $sql);
        $no=0;
        while($siswa=mysqli_fetch_array($query)){
            $no++;
        echo "<tr>";
        echo "<td>".$no."</td>";
        echo "<td>".$siswa['nama']."</td>";
        echo "<td>".$siswa['tempat_lahir']."</td>";
        echo "<td>".$siswa['tanggal_lahir']."</td>";
        echo "<td>".$siswa['jurusan']."</td>";
        echo "<td>".$siswa['jenis_kelamin']."</td>";
        echo "<td>".$siswa['email']."</td>";
        echo "<td>".$siswa['no_handphone']."</td>";
        // echo "<td>";
        echo "</td>";
        
        }
    ?> 
  </tbody>
          </div>
        </div>
      </div>
    </div>
  </body>
</html>

 <script>
  window.print();
 </script>

</body>
</html>