<?php
include("crud.php");
if( !isset($_GET['id']) ){
    header('Location: list-siswa.php');
}
$id = $_GET['id'];
$sql = "SELECT * FROM calon_siswa WHERE id=$id";
$query = mysqli_query($db, $sql);
$siswa = mysqli_fetch_assoc($query);
if( mysqli_num_rows($query) < 1 ){
    die("data tidak ditemukan...");
}
?>
<!DOCTYPE html>
<html>
<head>
    <title>Herugroupschool</title>
    <link rel="shortcut icon" href="herugroups.png">
    <link href="dashboard.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="dashboard.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous"></head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
<a class="navbar-brand" href="#">
		<img src="herugroups.png" width="65" class="d-inlin-block align-top" alt="" loading="lazy">
	</a>
<span class="navbar-brand mb-0 h1">Herugroupschool</span>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Sejarah</a>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Lainnya
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="cetak.php">Cetak Data</a>
          <a class="dropdown-item" href="list-siswa.php">Data Siswa</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="logout.php">Keluar</a>
        </div>
      </li>
        <li class="nav-item">
            <a class="nav-link disabled" href="#" tabindex="-1" aria-disabled="true">Coming soon</a>
        </li>
    </ul>
    <form class="form-inline my-2 my-lg-0">
      <input class="nav-link disabled" type="search" placeholder="Search" aria-label="Search">
      <button class="nav-link disabled" type="submit">Search</button>
    </form>
  </div>
</nav>
    
<div class="container">
    <div class="row">
        <div class="col-sm-9 col-sm-offset-5 col-md-12 col-md-offset-5 main">
        <center><h3>Formulir Edit Data</h3></center>
    <form action="proses-edit.php" method="POST">
        <div class="form-group">
            <input type="hidden" name="id" value="<?php echo $siswa['id'] ?>" />
            <label for="nama">Nama: </label>
            <input type="text" name="nama" placeholder="nama" class="form-control" value="<?php echo $siswa['nama'] ?>" />
        </div>
        <div class="form-group">
            <label for="tempat_lahir">Tempat Lahir: </label>
            <input type="text" name="tempat_lahir" placeholder="tempat lahir" class="form-control" value="<?php echo $siswa['tempat_lahir'] ?>" />
        </div>
        <div class="form-group">
            <label for="tanggal_lahir">Tanggal Lahir: </label>
            <input type="text" name="tanggal_lahir" placeholder="tanggal lahir" class="form-control" value="<?php echo $siswa['tanggal_lahir'] ?>" />
        </div>
        <div class="form-group">
            <label for="jurusan">Jurusan: </label>
            <?php $jurusan = $siswa['jurusan']; ?>
            <select class="form-control" name="jurusan">
                <option <?php echo ($jurusan == 'Web Developer') ? "selected": "" ?>>Web Developer</option>
                <option <?php echo ($jurusan == 'Desain') ? "selected": "" ?>>Desain</option>
                <option <?php echo ($jurusan == 'Sistem Informasi') ? "selected": "" ?>>Sistem Informasi</option>
                <?php echo $siswa['jurusan'] ?>
            </select>
        </div>
        <div class="form-group">
            <label for="jenis_kelamin">Jenis Kelamin: </label>
            <?php $jenis_kelamin = $siswa['jenis_kelamin']; ?>
            <select class="form-control" name="jenis_kelamin">
            <option <?php echo ($jenis_kelamin == 'laki-laki') ? "selected": "" ?>> Laki-laki</option>
            <option <?php echo ($jenis_kelamin == 'perempuan') ? "selected": "" ?>> Perempuan</option>
            <?php echo $siswa['jenis_kelamin'] ?>
        </select>
        </div>
        <div class="form-group">
            <label for="email">Email: </label>
            <input type="text" name="email" placeholder="email" class="form-control" value="<?php echo $siswa['email'] ?>" />
        </div>
        <div class="form-group">
            <label for="no_handphone">No Handphone: </label>
            <input type="text" name="no_handphone" placeholder="no handphone" class="form-control" value="<?php echo $siswa['no_handphone'] ?>" />
        </div>
            <button type="submit" value="Simpan" class="btn btn-primary" name="simpan">Save</button>
        </fieldset>
    </form>
    </body>
    </div>
    </div>
</div>
</html>